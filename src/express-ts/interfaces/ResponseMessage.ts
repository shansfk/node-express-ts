import { ContentType } from "../enums/ContentType";

export interface ResponseMessage {
  content: any,
  contentType: ContentType
}