export enum ContentType {
  json,
  text,
  xml,
  html
}