
import { ParamType } from "../enums/ParamType";

export interface ParamMetadata {
  type: ParamType,
  controllerName: string,
  methodName: string,
  index: number
}