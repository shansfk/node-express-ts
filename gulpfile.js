
// Reference : https://github.com/pgrm/vimfika/blob/master/gulpfile.js

var run = require('gulp-run');
var gulp = require('gulp');
var watch = require('gulp-watch');
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var sourcemaps = require('gulp-sourcemaps');

gulp.task('templates', function () {
  return gulp.src('src/views/**/*.html')
    .pipe(gulp.dest('dist/views'));
});

gulp.task('typescript', function () {
  const tsResult = gulp.src('src/**/*.ts')
    .pipe(run('tsc -w'));
});

gulp.task('watch', function () {
  gulp.watch('src/views/**/*.html', ['templates']);
});

gulp.task('build', ['typescript', 'templates'])

gulp.task('default', ['build', 'watch']);