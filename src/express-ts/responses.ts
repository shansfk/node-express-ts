
import { ResponseMessage } from "./interfaces/ResponseMessage";
import { ContentType } from "./enums/ContentType";

export function HtmlResponse(content: string): ResponseMessage {
  return { content: content, contentType: ContentType.html };
}

export function JsonResponse(content: object): ResponseMessage {
  return { content: content, contentType: ContentType.json };
}

export function TextResponse(content: string): ResponseMessage {
  return { content: content, contentType: ContentType.text };
}