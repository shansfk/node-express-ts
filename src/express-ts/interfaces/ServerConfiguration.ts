export interface ServerConfiguration {
  controllersPath?: string,
  schemaPath: string,
  modelsPath: string,
  schemsInterfacePath: string
}