import { User, IUser } from './../models/user';

import { Request, Response } from 'express';

import { Get, Req, Res, HtmlResponse, TextResponse, JsonResponse, Post, GlobalObjects } from "../express-ts";

import mongoose = require("mongoose");

export class SampleController {

  @Get('/')
  public Home( @Req() req: Request, @Res() res: Response) {
    res.send({ "name": "shan sfk" });
  }

  @Get('/json')
  public SendJson( @Req() req: Request) {
    return JsonResponse({ name: "oops" });
  }

  @Get('/savedata')
  public SaveData( @Res() res: Response) {

    let user: IUser = {
      email: "foo@bar.com",
      firstName: "Brian",
      lastName: "Love",
      indexNo: 1
    };

    new User(user).save(function (err, result, numAffected) {
      res.send({ data: result, rowsAffected: numAffected });
    });
  }
}