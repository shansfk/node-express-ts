export enum ParamType {
  Request,
  Response,
  Next
}