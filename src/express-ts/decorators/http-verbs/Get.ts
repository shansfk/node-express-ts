import { error } from 'util';
import * as express from 'express';

import { GlobalObjects } from "../../GlobalObjects";
import { RequestType } from '../../enums/RequestType';
import { ParamType } from "../../enums/ParamType";

export function Get(route: string): Function {
    return function (object: Object, methodName: string, descriptor: PropertyDescriptor) {

        const actionMetadata = {
            controllerName: object.constructor.name,
            methodName: methodName,
            type: RequestType.get,
            functionToExecute: descriptor.value,
            route: route
        };

        GlobalObjects.AddActionsMetadata(actionMetadata);
    };
};
