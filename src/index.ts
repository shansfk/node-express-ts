import { Server } from './express-ts';
import { RestUtils } from "./libs/RestUtils";

const server: Server = new Server({
  controllersPath: null,
  modelsPath: "./models",
  schemaPath: "./schemas",
  schemsInterfacePath: "./interfaces"
});

server.StartServer();