import { RequestType } from './enums/RequestType';
import { ActionMetadata } from './metadata/ActionMetadata';
import { ParamMetadata } from './metadata/ParamMetadata';

import mongoose = require("mongoose");

export class GlobalObjects {

  private static metadataStorage = (global as any).metadataStorage;

  private static paramMetadata: ParamMetadata[] = [];

  private static actionMetadata: ActionMetadata[] = [];

  private static server;

  private static mongooseConn;
  
  public static GetActionMetadata(): ActionMetadata[] {
    return GlobalObjects.actionMetadata;
  }

  public static GetParamMetadata(): ParamMetadata[] {
    return GlobalObjects.paramMetadata;
  }

  public static SetMongooseConnection(mongooseConn: mongoose.Connection) {
    GlobalObjects.mongooseConn = mongooseConn;
  }
 
  public static GetMongooseConnection(): mongoose.Connection  {
    return GlobalObjects.mongooseConn;
  }
  
  public static SetServer(server) {
    GlobalObjects.server = server;
  }

  public static AddActionsMetadata(methodMetadata: ActionMetadata) {
    GlobalObjects.actionMetadata.push(methodMetadata);
  }

  public static AddParamsMetadata(paramMetadata: ParamMetadata) {
    GlobalObjects.paramMetadata.push(paramMetadata);
  }
}