import { IUser } from './user';
import { GlobalObjects } from './../express-ts/GlobalObjects';
import { Document, Schema, Model } from "mongoose";

import mongoose = require("mongoose");

export interface IUser {
  email?: string;
  firstName?: string;
  lastName?: string;
  indexNo: Number;
}

export interface IUserModel extends Document, IUser {
  //custom methods for your model would be defined here
  findAllByEmail(email: string): Promise<IUser>;
}

const userSchema: Schema = new Schema({
  createdAt: Date,
  email: String,
  firstName: String,
  lastName: String
});

userSchema.pre("save", function (next) {
  if (!this.createdAt) {
    this.createdAt = new Date();
  }
  next();
});

// Custom method
userSchema.static("findAllByEmail", (email: string) => {

    return User
        .find({ email: email})
        .lean()
        .exec();
});

export type UserModel = Model<IUserModel> & IUserModel & IUser;

export const User: UserModel = <UserModel>GlobalObjects.GetMongooseConnection().model<IUserModel>("User", userSchema);