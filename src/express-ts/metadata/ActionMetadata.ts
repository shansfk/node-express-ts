import { RequestType } from '../enums/RequestType';

export interface ActionMetadata {
  type: RequestType
  methodName: string,
  controllerName: string,
  functionToExecute: Function,
  route: string
}