
import * as request from 'request';
import * as rp from 'request-promise';

export class RestUtils {

  public static async Get(uri: string) {
    try {
      return await rp(uri);
    } catch (error) {
      throw new error("Couldn't get the data from the requested url");
    }
  }

  public static async Post(uri: string, data: object) {
    try {
      let options = {
        method: 'POST',
        uri: uri,
        body: data,
        json: true // Automatically stringifies the body to JSON
      };
      return await rp(options);
    } catch (error) {
      throw new error("Got an error while posting");
    }
  }
}