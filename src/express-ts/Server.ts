import * as express from 'express';
import * as path from "path";
import * as cookieParser from "cookie-parser";
import * as bodyParser from 'body-parser';

import methodOverride = require("method-override");
import mongoose = require("mongoose");

import { GlobalObjects } from "./GlobalObjects";
import { ActionMetadata } from "./metadata/ActionMetadata";
import { ParamMetadata } from "./metadata/ParamMetadata";
import { ParamType } from "./enums/ParamType";
import { RequestType } from "./enums/RequestType";

import { Request, Response } from 'express';
import { ResponseMessage } from "./interfaces/ResponseMessage";
import { ContentType } from "./enums/ContentType";

import { Config } from '../config/Config';
import { ServerConfiguration } from "./interfaces/ServerConfiguration";
import { Utils } from "../libs/Utils";

export class Server {

  private server = express();

  constructor(private config: ServerConfiguration) {
    this.setConfig();
    this.initializeDb();
    this.importControllers();
    this.registerRouters();
  }

  public StartServer(): void {
    this.server.listen(3000, function () {
      console.log('Example app listening on port 3000!')
    });
  }

  private initializeDb(): void {
    //use q promises
    global.Promise = require("q").Promise;
    mongoose.Promise = global.Promise;

    //connect to mongoose
    let connection: mongoose.Connection = mongoose.createConnection(Config.GetMongoDbConfig().connectionString);
    GlobalObjects.SetMongooseConnection(connection);
  }

  private setConfig(): void {
    GlobalObjects.SetServer(this.server);
    if (!this.config.controllersPath) {
      this.config.controllersPath = __dirname + "/../controllers/*{.js,.ts}";
    }

    //configure pug
    this.server.set("views", path.join(__dirname, "views"));
    this.server.set("view engine", "pug");

    //mount cookie parker
    this.server.use(cookieParser("SECRET_GOES_HERE"));

    //mount override
    this.server.use(methodOverride());

    this.server.use(bodyParser.json()); // for parsing application/json
    this.server.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

  }

  private registerRouters(): void {
    const self = this;

    GlobalObjects.GetActionMetadata().forEach((o) => {
      self.server[RequestType[o.type]](o.route, function (req: Request, res: Response, next: Function) {

        const paramsMetadata = GlobalObjects.GetParamMetadata()
          .filter(p =>
            p.controllerName === o.controllerName &&
            p.methodName === o.methodName)
          .sort((o, o2) => o.index - o2.index);

        const params = paramsMetadata.map(s => {
           if (s.type === ParamType.Request) {
            return req;
          } else if (s.type === ParamType.Response) {
            return res;
          } else {
            return next;
          }
        });

        const response = o.functionToExecute.apply(this, params);
       
        self.handleResponse(response, res, next);
      });
    });
  }

  private handleResponse(response: ResponseMessage, res: Response, next: Function) {
    if (typeof response === 'undefined' || typeof response.contentType === 'undefined') {
      return;
    }

    if (response.contentType === ContentType.json) {
      res.json(response.content);
    } else if (response.contentType === ContentType.html) {
      res.sendFile(response.content, { root: __dirname + '/../views' });
    } else if (response.contentType === ContentType.text) {
      res.send(response.content.toString());
    } else {
      next();
    }
  }

  private importSchemas(): void {
    Utils.ImportModules(this.config.schemsInterfacePath);
    Utils.ImportModules(this.config.modelsPath);
    Utils.ImportModules(this.config.schemaPath);
  }

  private importControllers(): void {
    Utils.ImportModules(this.config.controllersPath);
  }
}