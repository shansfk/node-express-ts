import * as path from "path";

export class Utils {

  private static formats = [".js", ".ts"];

  public static ImportModules(dir: string): void {
    const allFiles = require("glob").sync(path.normalize(dir));
    allFiles.filter(file => {
      const dtsExtension = file.substring(file.length - 5, file.length);
      return Utils.formats.indexOf(path.extname(file)) !== -1 && dtsExtension !== ".d.ts";
    })
      .map(file => {
        require(file);
      });
  }

  // https://stackoverflow.com/questions/5999998/how-can-i-check-if-a-javascript-variable-is-function-type
  public static IsFunction(functionToCheck): boolean {
    var getType = {};
    if (!functionToCheck) {
      return false;
    }
    var type = getType.toString.call(functionToCheck).toString();
    return type === '[object Function]';
  }
}