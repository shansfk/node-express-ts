import { IMongodbConfig } from "./IMongodbConfig";
import { MongodbConfig } from './mongodb';

export class Config {
  
  public static GetMongoDbConfig(): IMongodbConfig {
    console.dir((<IMongodbConfig>MongodbConfig).connectionString);
    return <IMongodbConfig>MongodbConfig;
  }
}