# nodejs-express-mongoose starter

This is a boilerplate project which helps to setup nodejs-express-mongoose easily using typescript. 
This uses typescript's decorator feature and provides the attribute style routing to the controllers like in the flask framework. 
At the moment this project is not completed. It has only Get, Post support. All the http-verbs support will be added soon. 
Already a full fledged starter of this attribute style routing is implemented [here](https://github.com/pleerock/routing-controllers).

This is inspired by that project and an attempt to learn express-js and typescript I am implementing most of the 
functionalities in that on my own with authentication using jwt, moongoose and logger support.
    
# To run

```sh
$ cd node-express-ts
$ npm install
$ npm start
```
note: Install typescript, concurrently, gulp globally.

If you are using visual studio code then run only ```$ gulp``` in the termianl and run the project in visual studio code. The launch.json file is added so debugging is possible that way.