import { GlobalObjects } from "../GlobalObjects";
import { RequestType } from '../enums/RequestType';
import { ParamType } from "../enums/ParamType";

export function Req(): Function {
  return function (object: Object, methodName: string, index: number) {
    GlobalObjects.AddParamsMetadata({
      controllerName: object.constructor.name,
      index: index,
      methodName: methodName,
      type: ParamType.Request
    });
  };
}